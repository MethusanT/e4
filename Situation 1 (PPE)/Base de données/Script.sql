-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 30 avr. 2019 à 08:09
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
 Create DATABASE methuppe
 
 go
 
 use methuppe
--

-- --------------------------------------------------------

--
-- Structure de la table `demande_d_un_voeux`
--

DROP TABLE IF EXISTS `demande_d_un_voeux`;
CREATE TABLE IF NOT EXISTS `demande_d_un_voeux` (
  `id_utilisateur` int(11) NOT NULL,
  `id_Voeux` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_Voeux`),
  KEY `Demande_D_un_voeux_VOEUX0_FK` (`id_Voeux`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `intervenir`
--

DROP TABLE IF EXISTS `intervenir`;
CREATE TABLE IF NOT EXISTS `intervenir` (
  `id_Voeux` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`id_Voeux`,`id_utilisateur`),
  KEY `Intervenir_Utilisateur0_FK` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nbPlace` int(11) NOT NULL,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `statut_utilisateur`
--

DROP TABLE IF EXISTS `statut_utilisateur`;
CREATE TABLE IF NOT EXISTS `statut_utilisateur` (
  `id_statut` int(11) NOT NULL AUTO_INCREMENT,
  `nom_statut` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_statut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `prenom` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `login` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Mot_de_passe` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `DateEntre` date NOT NULL,
  `id_statut` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`),
  KEY `Utilisateur_Statut_utilisateur_FK` (`id_statut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `voeux`
--

DROP TABLE IF EXISTS `voeux`;
CREATE TABLE IF NOT EXISTS `voeux` (
  `id_Voeux` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_Region` int(11) NOT NULL,
  `Ordre` int(11) NOT NULL,
  `DateCreation` date NOT NULL,
  `DateIntervention` date NOT NULL,
  `id_Region_CHOISIR` int(11) NOT NULL,
  PRIMARY KEY (`id_Voeux`),
  KEY `VOEUX_Region_FK` (`id_Region_CHOISIR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `demande_d_un_voeux`
--
ALTER TABLE `demande_d_un_voeux`
  ADD CONSTRAINT `Demande_D_un_voeux_Utilisateur_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `Demande_D_un_voeux_VOEUX0_FK` FOREIGN KEY (`id_Voeux`) REFERENCES `voeux` (`id_Voeux`);

--
-- Contraintes pour la table `intervenir`
--
ALTER TABLE `intervenir`
  ADD CONSTRAINT `Intervenir_Utilisateur0_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `Intervenir_VOEUX_FK` FOREIGN KEY (`id_Voeux`) REFERENCES `voeux` (`id_Voeux`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `Utilisateur_Statut_utilisateur_FK` FOREIGN KEY (`id_statut`) REFERENCES `statut_utilisateur` (`id_statut`);

--
-- Contraintes pour la table `voeux`
--
ALTER TABLE `voeux`
  ADD CONSTRAINT `VOEUX_Region_FK` FOREIGN KEY (`id_Region_CHOISIR`) REFERENCES `region` (`id_Region`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
